package com.umg.Reactivo;

import dto.Vehiculos;

import rx.Observable;
import rx.functions.Func2;


import java.util.ArrayList;
import java.util.List;

public class transportes {

        public static void main(String [] args){
            List<Vehiculos> vehiculos = new ArrayList<>();
            vehiculos.add(new Vehiculos("V_Simple", 300));
            vehiculos.add(new Vehiculos("V_Simple_aut", 300));
            vehiculos.add(new Vehiculos("V_DobleTraccion", 200));
            vehiculos.add(new Vehiculos("V_alta_Gama", 800));
            vehiculos.add(new Vehiculos("Motocicleta", 250));

            Observable miobservable =
                    Observable
                            .from(vehiculos.toArray())
                            .map((result) ->{
                                Vehiculos vehiculos1 = (Vehiculos) result;
                                return vehiculos1.getPrecio();
                            })
                            .reduce(
                                new Func2<Integer, Integer, Integer>() {
                                @Override
                                public Integer call(Integer acumulador, Integer actual) {
                                    // System.out.println("Actual:"+actual);
                                    return acumulador + actual;
                                }
                            }
            );

            miobservable.subscribe((sumatoria) -> {
                System.out.println("sumatoria -> " + sumatoria);

            });
        }
}



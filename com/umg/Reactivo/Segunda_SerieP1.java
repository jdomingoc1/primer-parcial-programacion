package com.umg.Reactivo;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

public class Segunda_SerieP1 {
        public static void main(String[] args){

            Integer [] Valores = {2, 5, 6, 8, 10, 35, 2, 10,};

            Observable.from(Valores).filter(
                    new Func1<Integer, Boolean>(){
                    @Override
                        public Boolean call(Integer t){
                            return t.equals(10);
                    }
                    }
            )
                    .sorted()
                    .subscribe(
                        new Action1<Integer>(){
                        @Override
                            public void call(Integer s){

                            System.out.println("Valor " + s);
                        }
                        }
                    );
        }
}

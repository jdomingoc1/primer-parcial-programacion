package com.umg.Reactivo;

import rx.Observable;
import rx.functions.Func2;

public class Segunda_SerieP2 {
    public static void main(String[] args){
        Integer[] Valores = {2, 5, 6, 8, 10, 35, 2, 10};



            Observable miobservable = Observable.from(Valores).reduce(
            new Func2<Integer, Integer, Integer>(){
            @Override
                public Integer call(Integer sum, Integer actual){
                    return sum+actual;
                 }
            }
           );

            miobservable.subscribe((sumatoria) ->{
                System.out.println("Sumatoria " + sumatoria);
            });

    }
}

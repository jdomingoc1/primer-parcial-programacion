package com.umg.Reactivo;

import dto.Valores;
import rx.Observable;
import java.util.ArrayList;
import java.util.List;


import rx.observables.MathObservable;

public class Segunda_Serie {

    public static void main(String[] args){
        List<Valores> valores = new ArrayList<>();
        valores.add(new Valores(1,2));
        valores.add(new Valores(2,5));
        valores.add(new Valores(3,6));
        valores.add(new Valores(4,8));
        valores.add(new Valores(5,10));
        valores.add(new Valores(6,35));
        valores.add(new Valores(7,2));
        valores.add(new Valores(8,10));

        Observable<Valores> valoresObservable = Observable.from(valores);

        MathObservable
                .from(valoresObservable)
                .averageDouble(Valores::getValor)
                .subscribe((promedio) ->{
                    System.out.println("Promedio: " + promedio);
                });
    }
}
